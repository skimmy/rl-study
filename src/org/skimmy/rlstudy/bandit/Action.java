// Copyright 2022 Michele Schimd

// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at

//     http://www.apache.org/licenses/LICENSE-2.0

// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package org.skimmy.rlstudy.bandit;

import java.util.Random;

public class Action {
    public static int greedyAction(double[] Q, double exploreProbability, Random rd) {
        int iMax = 0;
        for (int i = 1; i < Q.length; i++) {
            if (Q[i] > Q[iMax]) {
                iMax = i;
            }
        }
        return (rd.nextDouble() > exploreProbability) ? iMax : rd.nextInt(Q.length);
    }
}