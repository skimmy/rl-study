// Copyright 2022 Michele Schimd

// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at

//     http://www.apache.org/licenses/LICENSE-2.0

// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package org.skimmy.rlstudy.bandit;

import java.util.Random;

/**
 * A simple bandit that returns as reward a normal distributed value
 * with mean and standard deviation set in the constructor
 * 
 * @author Michele Schimd
 */
public class SimpleBandit implements IBandit {

    private double mean;
    private double sd;
    private Random rd;

    public SimpleBandit(double mean, double sd) {
        this.mean = mean;
        this.sd = sd;
        this.rd = new Random();
    }

    @Override
    public double reward() {
        return (mean + rd.nextGaussian()) / sd;
    }

}
