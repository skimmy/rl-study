// Copyright 2022 Michele Schimd

// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at

//     http://www.apache.org/licenses/LICENSE-2.0

// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package org.skimmy.rlstudy.bandit;

import java.util.Random;

public class MultiArmedBandits {
    private int k;
    private IBandit[] bandits;

    public MultiArmedBandits(int k, double[] means, double[] stds) {
        this.k = k;
        bandits = new SimpleBandit[k];
        for (int i = 0; i < k; i++) {
            bandits[i] = new SimpleBandit(means[i ], stds[i]);
        }
    }

    public int getK() {
        return k;
    }

    public IBandit[] getBandits() {
        return this.bandits;
    }

    public void simulate(int steps, Random rd) {
        // TODO: implement the equation (2.3) in RL Book (pag. 31)
        double exploreProbability = 0.5;
        double[] Q = new double[k];
        int[] actionCount = new int[k];
        double totalReward = 0;
        for (int t = 0; t < steps; t++) {
            int action = Action.greedyAction(Q, exploreProbability, rd);
            actionCount[action]++;
            double reward = bandits[action].reward();
            Q[action] += reward;
            totalReward += reward;
        }
        System.out.println("Total reward " + totalReward);
        for (int i = 0; i < Q.length; i++) {
            System.out.println(Q[i] / actionCount[i]);
        }
    }
}
